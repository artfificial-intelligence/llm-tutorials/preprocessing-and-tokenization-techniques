# 전처리와 토큰화 기법<sup>[1](#footnote_1)</sup>

> <font size="3">언어 모델에 대한 텍스트 데이터를 전처리하고 토큰화하는 방법을 배운다.</font>

## 목차

1. [개요](./preprocessing-and-tokenization-techniques.md#intro)
1. [전처리란 무엇이며 왜 중요한가?](./preprocessing-and-tokenization-techniques.md#sec_02)
1. [일반적인 전처리 단계](./preprocessing-and-tokenization-techniques.md#sec_03)
1. [토큰화란 무엇이며 왜 중요한가?](./preprocessing-and-tokenization-techniques.md#sec_04)
1. [일반적인 토큰화 기법](./preprocessing-and-tokenization-techniques.md#sec_05)
1. [적합한 전처리 및 토큰화 방법의 선택](./preprocessing-and-tokenization-techniques.md#sec_06)
1. [요약](./preprocessing-and-tokenization-techniques.md#summary)

<a name="footnote_1">1</a>: [LLM Tutorial 3 — Preprocessing and Tokenization Techniques](https://ai.plainenglish.io/llm-tutorial-3-preprocessing-and-tokenization-techniques-7b3682fc6d1f?sk=eafce219dd57d03f57c16f09e11d69e4)를 편역하였다.
